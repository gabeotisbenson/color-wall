import DefaultLayout from '~/layouts/Default';
import store from '~/modules/store';
import Vue from 'vue';

const app = new Vue({
	...DefaultLayout,
	name: 'Colorwall',
	store
}).$mount('#cell-wrapper');

export default app;
