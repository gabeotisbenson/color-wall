const { MAX_SAFE_INTEGER } = Number;

export const randomInt = (min = 0, max = MAX_SAFE_INTEGER) => {
	const normalizedMin = Math.ceil(min);
	const normalizedMax = Math.floor(max);

	return Math.floor(Math.random() * (normalizedMax - normalizedMin + 1)) + normalizedMin;
};
