import Color from 'color';
import { randomInt } from '~/utilities/number';

const MAX_COLOR_VALUE = 255;

export const randomColor = () => Color.rgb([1, 1, 1].map(() => randomInt(0, MAX_COLOR_VALUE)));
