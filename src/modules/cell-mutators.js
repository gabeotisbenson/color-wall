import { randomColor } from '~/utilities/color';
import { randomInt } from '~/utilities/number';

// eslint-disable-next-line no-magic-numbers
export const darken = color => color.darken(randomInt(1, 10) / 100);

// eslint-disable-next-line no-magic-numbers
export const lighten = color => color.lighten(randomInt(1, 10) / 100);

export const blend = (color, { north, east, south, west }) => color
	.mix(north)
	.mix(east)
	.mix(south)
	.mix(west);

export const random = (color, { north, east, south, west }) => {
	const RANDOM_THRESHOLD = 0.005;

	const num = Math.random();
	const nonTrulyRandomChances = [north, east, south, west];

	if (num < RANDOM_THRESHOLD) return randomColor();

	if (num === RANDOM_THRESHOLD) return color;

	return nonTrulyRandomChances[randomInt(0, nonTrulyRandomChances.length - 1)];
};

export const negate = color => color.negate();
