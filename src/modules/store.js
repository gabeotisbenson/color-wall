import Color from 'color';
import { randomColor } from '~/utilities/color';
import { randomInt } from '~/utilities/number';
import Vue from 'vue';
import {
	blend,
	darken,
	lighten,
	random
} from '~/modules/cell-mutators';
import Vuex, { Store } from 'vuex';

Vue.use(Vuex);

const DEFAULT_WIDTH = 5;
const DEFAULT_HEIGHT = DEFAULT_WIDTH;
const DEFAULT_MUTATION_INTERVAL_IN_MS = 10;
const DEFAULT_TRANSITION_DURATION_IN_MS = 100;
const MUTATIONS = [blend, darken, lighten, random];

const newCell = color => {
	let newColor = randomColor();

	if (color) {
		const modAmount = randomInt(1, 100) / 100;
		const mode = randomInt(-1, 1);
		if (mode === 1) newColor = color.lighten(modAmount);
		else if (mode === -1) newColor = color.darken(modAmount);
		else newColor = color;
	}

	return newColor;
};

const generateCells = (baseColor, height = DEFAULT_HEIGHT, width = DEFAULT_WIDTH) => new Array(height * width)
	.fill(null)
	.map(() => newCell(baseColor));

const getNeighborIndices = (index, { width, height }) => {
	let isTop = false;
	let isRight = false;
	let isBottom = false;
	let isLeft = false;

	if (index < width) isTop = true;
	else if (index >= width * (height - 1)) isBottom = true;
	if (index % width === 0) isLeft = true;
	else if (index % width === width - 1) isRight = true;

	const indices = {
		nI: isTop ? index : index - width,
		eI: isRight ? index : index + 1,
		sI: isBottom ? index : index + width,
		wI: isLeft ? index : index - 1
	};

	return indices;
};

const getRandomMutation = () => MUTATIONS[randomInt(0, MUTATIONS.length - 1)];

const store = new Store({
	state: {
		baseColor: null,
		cells: generateCells(),
		width: DEFAULT_WIDTH,
		height: DEFAULT_HEIGHT,
		mutationInterval: DEFAULT_MUTATION_INTERVAL_IN_MS,
		intervalId: null,
		transitionDuration: DEFAULT_TRANSITION_DURATION_IN_MS
	},
	mutations: {
		mutateCell (state, options) {
			const selectedIndexExistsAndIsValid = options && 'index' in options && options.index >= 0 && options.index < state.cells.length;
			const index = selectedIndexExistsAndIsValid ? options.index : randomInt(0, state.cells.length - 1);

			const specifiedMutatorExistsAndIsValid = options && 'mutator' in options && typeof options.mutator === 'function';
			const mutator = specifiedMutatorExistsAndIsValid ? options.mutator : getRandomMutation();

			const { nI, eI, sI, wI } = getNeighborIndices(index, state);
			const dupArr = [...state.cells];

			dupArr[index] = mutator(state.cells[index], {
				north: state.cells[nI],
				east: state.cells[eI],
				south: state.cells[sI],
				west: state.cells[wI]
			});

			state.cells = dupArr;
		},
		randomizeCell (state, cellIndex) {
			const index = cellIndex >= 0 && cellIndex < state.cells.length ? cellIndex : randomInt(0, state.cells.length - 1);
			state.cells[index] = newCell();
		},
		setBaseColor (state, color) {
			// eslint-disable-next-line new-cap
			if (color) state.baseColor = Color(color);
			else state.baseColor = null;

			state.cells = generateCells(state.baseColor, state.height, state.width);
		},
		setHeight (state, height = DEFAULT_HEIGHT) {
			if (isNaN(height)) return;
			if (height < 1) return;
			state.height = height;
			state.cells = generateCells(state.baseColor, state.height, state.width);
		},
		setWidth (state, width = DEFAULT_WIDTH) {
			if (isNaN(width)) return;
			if (width < 1) return;
			state.width = width;
			state.cells = generateCells(state.baseColor, state.height, state.width);
		},
		setTransitionDuration (state, duration = DEFAULT_TRANSITION_DURATION_IN_MS) {
			if (isNaN(duration)) return;
			if (duration < 1) return;
			state.transitionDuration = duration;
		}
	},
	actions: {
		setMutationInterval ({ state, dispatch }, interval = DEFAULT_MUTATION_INTERVAL_IN_MS) {
			if (isNaN(interval)) return;
			if (interval < 1) return;
			state.mutationInterval = interval;
			dispatch('updateInterval');
		},
		updateInterval ({ commit, state }) {
			const {
				mutationInterval,
				intervalId: oldIntervalId
			} = state;

			if (oldIntervalId) clearInterval(oldIntervalId);

			state.intervalId = setInterval(() => commit('mutateCell'), mutationInterval);
		}
	}
});

export default store;
